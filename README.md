# Bowtie Container Registry

This is a public repository to facilitate container image distribution outside of private repositories.
Please visit the [container registry](https://gitlab.com/bowtienet/registry/container_registry) page to view the list of available container images.

## [Registry](https://gitlab.com/bowtienet/registry/container_registry)

Two types of container images are available for download:

- Images suffixed with `-dev` are built from release candidates and are not suitable for production deployments.
  Use these only if you know the reason why you need to run a particular tag.
- Images without the `-dev` suffix are production-ready and published alongside their sibling Controller virtual machine images on the [Bowtie package download page](https://api.bowtie.works/packages/).

These container images are meant for use with the [Bowtie Helm chart as part of a Kubernetes-based installation](https://docs.bowtie.works/setup-controller.html#kubernetes-installation).
They expect the presence of some key files that the Helm chart `initContainer`s bootstrap and so you are encouraged to use the [Helm chart](https://docs.bowtie.works/setup-controller.html#helm-chart) rather than pulling the images directly.

See the [Bowtie documentation](https://docs.bowtie.works) for additional information.
